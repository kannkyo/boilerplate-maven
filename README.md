# boilerplate-maven

## GitHub

[![maven ci](https://github.com/kannkyo/boilerplate-maven/actions/workflows/maven-ci.yml/badge.svg)](https://github.com/kannkyo/boilerplate-maven/actions/workflows/maven-ci.yml) [![maven publish](https://github.com/kannkyo/boilerplate-maven/actions/workflows/maven-publish.yml/badge.svg)](https://github.com/kannkyo/boilerplate-maven/actions/workflows/maven-publish.yml) [![CodeQL](https://github.com/kannkyo/boilerplate-maven/actions/workflows/codeql-analysis.yml/badge.svg)](https://github.com/kannkyo/boilerplate-maven/actions/workflows/codeql-analysis.yml)

## GitLab
